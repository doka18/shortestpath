package ru.sber.jd.utils;

import ru.sber.jd.dto.StationDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBUtils {
    public static Connection createConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:~/testShortPath",
                "sa", "");
    }

    public static void createTable(Connection connection, String table) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(String.format("create table if not exists %s(id int not null AUTO_INCREMENT, name text, neighbor_1 text, neighbor_2 text, " +
                "neighbor_3 text, neighbor_4 text, neighbor_5 text, neighbor_6 text)", table));
        connection.commit();
    }

    public static void dropTable(Connection connection, String table) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(String.format("drop table if exists %s", table));
        connection.commit();
    }

    public static void insertStation(Connection connection, StationDto stationDto) throws SQLException {
        Statement statement = connection.createStatement();
        switch(stationDto.getNeighbors().size()){
            case 0:
                statement.execute(String.format("insert into stations (name) values ('%s')"
                        , stationDto.getName()));
                break;
            case 1:
                statement.execute(String.format("insert into stations (name, neighbor_1) values ('%s', '%s')"
                        , stationDto.getName(), stationDto.getNeighbors().get(0).getName()));
                break;
            case 2:
                statement.execute(String.format("insert into stations (name, neighbor_1, neighbor_2) values ('%s', '%s', '%s')"
                        , stationDto.getName(), stationDto.getNeighbors().get(0).getName(), stationDto.getNeighbors().get(1).getName()));
                break;
            case 3:
                statement.execute(String.format("insert into stations (name, neighbor_1, neighbor_2, neighbor_3) values ('%s', '%s', '%s', '%s')"
                        , stationDto.getName(), stationDto.getNeighbors().get(0).getName(), stationDto.getNeighbors().get(1).getName(), stationDto.getNeighbors().get(2).getName()));
                break;
            case 4:
                statement.execute(String.format("insert into stations (name, neighbor_1, neighbor_2, neighbor_3, neighbor_4) values ('%s', '%s', '%s', '%s', '%s')"
                        , stationDto.getName(), stationDto.getNeighbors().get(0).getName(), stationDto.getNeighbors().get(1).getName(), stationDto.getNeighbors().get(2).getName(),
                        stationDto.getNeighbors().get(3).getName()));
                break;
            case 5:
                statement.execute(String.format("insert into stations (name, neighbor_1, neighbor_2, neighbor_3, neighbor_4, neighbor_5) values ('%s', '%s', '%s', '%s', '%s', '%s')"
                        , stationDto.getName(), stationDto.getNeighbors().get(0).getName(), stationDto.getNeighbors().get(1).getName(), stationDto.getNeighbors().get(2).getName(),
                        stationDto.getNeighbors().get(3).getName(), stationDto.getNeighbors().get(4).getName()));
                break;
            case 6:
                statement.execute(String.format("insert into stations (name, neighbor_1, neighbor_2, neighbor_3, neighbor_4, neighbor_5, neighbor_6) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                        , stationDto.getName(), stationDto.getNeighbors().get(0).getName(), stationDto.getNeighbors().get(1).getName(), stationDto.getNeighbors().get(2).getName(),
                        stationDto.getNeighbors().get(3).getName(), stationDto.getNeighbors().get(4).getName(), stationDto.getNeighbors().get(5).getName()));
                break;
        }
        connection.commit();
    }

    public static List<StationDto> selectAllStations(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from stations");
        List<StationDto> stations = new ArrayList<>();
        while(resultSet.next()){
            Integer id = resultSet.getInt(1);
            String name = resultSet.getString(2);
            String neighbor_1 = resultSet.getString(3);
            String neighbor_2 = resultSet.getString(4);
            String neighbor_3 = resultSet.getString(5);
            String neighbor_4 = resultSet.getString(6);
            String neighbor_5 = resultSet.getString(7);
            String neighbor_6 = resultSet.getString(8);
            StationDto station = new StationDto(name);
            if(neighbor_1 != null){
                station.addNeghbor(new StationDto(neighbor_1));
            }
            if(neighbor_2 != null){
                station.addNeghbor(new StationDto(neighbor_2));
            }
            if(neighbor_3 != null){
                station.addNeghbor(new StationDto(neighbor_3));
            }
            if(neighbor_4 != null){
                station.addNeghbor(new StationDto(neighbor_4));
            }
            if(neighbor_5 != null){
                station.addNeghbor(new StationDto(neighbor_5));
            }
            if(neighbor_6 != null){
                station.addNeghbor(new StationDto(neighbor_6));
            }
            stations.add(station);
        }
        return stations;
    }
}
