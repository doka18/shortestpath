package ru.sber.jd.utils;

import ru.sber.jd.dto.StationDto;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class LoadFromFileHelper {
    public static ArrayList<StationDto> loadStationFromFile() {
        ArrayList<StationDto> stations = new ArrayList<>();
        try {
            String userDirectory = System.getProperty("user.dir");
            userDirectory = userDirectory + "\\stationList.csv";
            File file = new File(userDirectory);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String[] splitLine = (scanner.nextLine()).split(",");
                StationDto stationDto = new StationDto(splitLine[0].toLowerCase());
                for (int i = 1; i < splitLine.length; i++) {
                    if (splitLine[i] != "") {
                        stationDto.addNeghbor(new StationDto(splitLine[i].toLowerCase()));
                    }
                }
                stations.add(stationDto);
            }
            return stations;
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return stations;
    }
}
