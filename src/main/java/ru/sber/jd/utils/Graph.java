package ru.sber.jd.utils;

import ru.sber.jd.dto.StationDto;

import javax.swing.plaf.IconUIResource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Graph {
    private Map<String, StationDto> stations = new HashMap<>();
    public Graph() {};

    public void loadFromList(ArrayList<StationDto> stations){
        for(StationDto station: stations){
            for(StationDto neighborStation: station.getNeighbors()){
                addEdge(station.getName().toLowerCase(), neighborStation.getName().toLowerCase());
            }
        }
    }

    public void addEdge(String firstStationName, String secondStationName){
        StationDto firstStation = stations.get(firstStationName);
        if(firstStation == null){
            firstStation = new StationDto(firstStationName);
        }

        StationDto secondStation = stations.get(secondStationName);
        if(secondStation == null){
            secondStation = new StationDto(secondStationName);
        }
        firstStation.addNeghbor(secondStation);
        secondStation.addNeghbor(firstStation);

        stations.put(firstStationName, firstStation);
        stations.put(secondStationName, secondStation);
    }

    public ArrayList<String> shortestPath(String srcRaw, String destRaw){
        String src = srcRaw.toLowerCase();
        String dest = destRaw.toLowerCase();
        Map<String, String> parents = new HashMap<>();
        ArrayList<StationDto> temp = new ArrayList<>();

        StationDto start = stations.get(src);
        temp.add(start);
        parents.put(src, null);
        try {
            while(temp.size() > 0){
                StationDto currentStation = temp.get(0);
                ArrayList<StationDto> neighbors = currentStation.getNeighbors();

                for(int i = 0; i < neighbors.size(); i++){
                    StationDto neighbor = neighbors.get(i);
                    String stationName = neighbor.getName();

                    boolean visited = parents.containsKey(stationName);
                    if(visited){
                        continue;
                    } else {
                        temp.add(neighbor);
                        parents.put(stationName, currentStation.getName());
                        if(stationName.equals(dest)){
                            return getPath(parents, dest);
                        }
                    }
                }
                temp.remove(0);
            }
        } catch (Exception e){
            return new ArrayList<String>();
        }
        return new ArrayList<String>();
    }

    private ArrayList<String> getPath(Map<String, String> parents, String dest){
        ArrayList<String> path = new ArrayList<>();
        String station = dest;
        while (station != null){
            path.add(0, station);
            String parent = parents.get(station);
            station = parent;
        }
        return path;
    }
}
