package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.jd.enities.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}