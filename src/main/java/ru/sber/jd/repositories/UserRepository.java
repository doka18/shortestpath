package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.jd.enities.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
