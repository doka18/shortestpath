package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.StationDto;
import ru.sber.jd.enities.Role;
import ru.sber.jd.enities.User;
import ru.sber.jd.repositories.RoleRepository;
import ru.sber.jd.repositories.UserRepository;
import ru.sber.jd.utils.DBUtils;
import ru.sber.jd.utils.Graph;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;

import static ru.sber.jd.utils.LoadFromFileHelper.loadStationFromFile;

@Service
@RequiredArgsConstructor
public class ShortestPathService implements CommandLineRunner {
    @Autowired
    private final RoleRepository roleRepository;
    @Autowired
    private final UserRepository userRepository;
    @Override
    public void run(String... args) throws Exception {
        addRole((long) 1, "ROLE_USER");
        addRole((long) 2, "ROLE_ADMIN");
        addAdmin();

        Connection connection = DBUtils.createConnection();
        DBUtils.dropTable(connection, "stations");
        DBUtils.createTable(connection, "stations");
        ArrayList<StationDto> stations = loadStationFromFile();
        for(StationDto station: stations){
            DBUtils.insertStation(connection, station);
        }
        connection.close();
    }

    public void addRole(Long id, String role){
        Role newRole = new Role(id, role);
        roleRepository.save(newRole);
    }

    public void addAdmin(){
        User newUser = new User();
        newUser.setUsername("admin");
        newUser.setRoles(Collections.singleton(new Role(2L, "ROLE_ADMIN")));
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        newUser.setPassword(passwordEncoder.encode("password"));
        userRepository.save(newUser);
    }
}
