package ru.sber.jd.dto;

import java.util.ArrayList;

public class StationDto {
    private String name;
    ArrayList<StationDto> neighbors = new ArrayList<>();
    public StationDto(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void addNeghbor(StationDto neighbor){
        neighbors.add(neighbor);
    }
    public ArrayList<StationDto> getNeighbors(){
        return neighbors;
    }
    public String toString() {
        return this.name;
    }
}
