package ru.sber.jd.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sber.jd.enities.User;
import ru.sber.jd.logger.LoggerKafkaProducer;
import ru.sber.jd.repositories.UserRepository;
import ru.sber.jd.services.UserService;

import java.util.ArrayList;

@Controller
public class AdminController {
    @Autowired
    LoggerKafkaProducer producer;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/admin")
    public String userList(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = new User();
        if(principal instanceof UserDetails){
            String username = ((UserDetails)principal).getUsername();
            user = userRepository.findByUsername(username);
            producer.sendMessage(user, "open admin page");
        }
        model.addAttribute("allUsers", userService.allUsers());
        return "admin";
    }

    @PostMapping("/admin")
    public String  deleteUser(@RequestParam(required = true, defaultValue = "" ) Long userId,
                              @RequestParam(required = true, defaultValue = "" ) String action,
                              Model model) {
        if (action.equals("delete")){
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            User user = new User();
            if(principal instanceof UserDetails){
                String username = ((UserDetails)principal).getUsername();
                user = userRepository.findByUsername(username);
                producer.sendMessage(user, "Delete user " + userService.findUserById(userId).getUsername());
            }
            userService.deleteUser(userId);
        }
        return "redirect:/admin";
    }

    @GetMapping("/admin/gt/{userId}")
    public String  gtUser(@PathVariable("userId") Long userId, Model model) {
        model.addAttribute("allUsers", userService.usergtList(userId));
        return "admin";
    }
}
