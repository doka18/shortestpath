package ru.sber.jd.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.Authentication;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.jd.enities.User;
import ru.sber.jd.logger.LoggerKafkaProducer;
import ru.sber.jd.repositories.UserRepository;
import ru.sber.jd.utils.DBUtils;
import ru.sber.jd.utils.Graph;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class Controllers {
    private final LoggerKafkaProducer producer;
    @Autowired
    private final UserRepository userRepository;
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(@RequestParam(value = "error", required = false) String error, @RequestParam(value = "logout", required = false) String logout, Model model) {
        String errorMessge = null;
        if(error != null) {
            errorMessge = "Username or Password is incorrect !!";
        }
        if(logout != null) {
            errorMessge = "You have been successfully logged out !!";
        }
        model.addAttribute("errorMessge", errorMessge);
        return "login";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){

            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout=true";
    }

    @RequestMapping(value = "/path", method = RequestMethod.GET)
    public ModelAndView findPath(@RequestParam("src") String src, @RequestParam("dest") String dest) throws SQLException {
        Connection connection = DBUtils.createConnection();
        ArrayList<String> result = new ArrayList<>();
        Graph metro = new Graph();
        metro.loadFromList(new ArrayList<>(DBUtils.selectAllStations(connection)));
        connection.close();
        result = metro.shortestPath(src.toLowerCase(), dest.toLowerCase());

        User user = new User();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof UserDetails){
            String username = ((UserDetails)principal).getUsername();
            user = userRepository.findByUsername(username);
        }
        if(result.size() == 0){
            result.add("Маршрут не найден");
        } else {
              if(principal instanceof UserDetails){
                  user.addHistory(result);
            }
        }
        userRepository.save(user);
        producer.sendMessage(user, new String(src + " -> " + dest));
        ModelAndView model = new ModelAndView("index");
        model.addObject("findedPath", result);
        return model;
    }

    @RequestMapping(value = "/searchHistory", method = RequestMethod.GET)
    public ModelAndView findHistory() throws SQLException {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ArrayList<ArrayList<String>> history = new ArrayList<>();
        User user = new User();
        if(principal instanceof UserDetails){
            String username = ((UserDetails)principal).getUsername();
            user = userRepository.findByUsername(username);
            history = user.getSearchHistory();
        }
        ModelAndView model = new ModelAndView("index");
        model.addObject("searchHistory", history);
        producer.sendMessage(user, "search history");
        return model;
    }
}
