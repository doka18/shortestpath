      <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
      <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
      <%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

      <!DOCTYPE HTML>
      <html>
      <head>
        <title>Главная</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
      </head>
      <body>
      <div>
        <h3>${pageContext.request.userPrincipal.name}</h3>
        <sec:authorize access="!isAuthenticated()">
          <h4><a href="/login">Войти</a></h4>
          <h4><a href="/registration">Зарегистрироваться</a></h4>
        </sec:authorize>
        <sec:authorize access="isAuthenticated()">
          <h4><a href="/logout">Выйти</a></h4>
          <div>
            <form method="GET" action="/path">
              <h2>Проложить маршрут</h2>
              <div>
                <input name="src" type="text" placeholder="Начальная станция"
                       autofocus="true"/>
                <input name="dest" type="text" placeholder="Конечная станция"/>
                <button type="submit">Рассчитать</button>
              </div>
            </form>
            <div>
              <p>${findedPath}</p>
            </div>
            <div>
              <p>
                <c:forEach items="${searchHistory}" var="record"><p>${record};</p></c:forEach>
              </p>
            </div>
            <h4><a href="/searchHistory">История поиска</a></h4>
        </sec:authorize>
      </div>
      </body>
      </html>